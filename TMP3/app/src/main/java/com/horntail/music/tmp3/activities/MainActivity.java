package com.horntail.music.tmp3.activities;


import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.MotionEvent;
import android.view.View;
import android.widget.RelativeLayout;
import android.widget.SeekBar;
import android.widget.TextView;

import com.horntail.music.tmp3.R;
import com.horntail.music.tmp3.fragments.MainFragment;
import com.horntail.music.tmp3.helpers.OnSwipeListener;

public class MainActivity extends AppCompatActivity {

    private static final String TAG = "mainActivity";
    RelativeLayout controllerReduced;
    int songId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //if instance exist don't recreate
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction().replace(R.id.container_main, new MainFragment()).commit();
        }
        //restore saved instance variables
        if (savedInstanceState != null) {
            if (savedInstanceState.getInt("bottomController", View.GONE) == View.VISIBLE) {
                showBottomController(savedInstanceState.getCharSequence("bottomControllerSongName", "").toString(),
                        savedInstanceState.getInt("bottomControllerSongId"));
            }
        }
        //initialize bottom controller
        controllerReduced = (RelativeLayout) findViewById(R.id.controller_reduced);
        //set swipeListener on bottom controller
        controllerReduced.setOnTouchListener(new BottomControllerSwipeListener());
    }

    //set bottom controller from other fragment
    public void showBottomController(String songName, int id) {
        findViewById(R.id.controller_reduced).setVisibility(View.VISIBLE);
        SeekBar seekBar = (SeekBar) findViewById(R.id.controller_reduced_seek);
        //disable manual seeking
        seekBar.setOnTouchListener(new View.OnTouchListener() {
            @Override
            public boolean onTouch(View v, MotionEvent event) {
                return true;
            }
        });
        seekBar.getThumb().mutate().setAlpha(0);
        seekBar.setProgress(100);
        ((TextView) findViewById(R.id.controller_reduced_song_name)).setText(songName);
        songId = id;
    }

    //toggle bottom controller visibility


    //save instance variable
    @Override
    public void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        //is bottom_controller visible
        savedInstanceState.putInt("bottomController", findViewById(R.id.controller_reduced).getVisibility());
        //save current song name
        savedInstanceState.putCharSequence("bottomControllerSongName", ((TextView) findViewById(R.id.controller_reduced_song_name)).getText());
        //save current song id
        savedInstanceState.putInt("bottomControllerSongId", songId);
    }

    //custom upward swipe listener for bottom controller
    private class BottomControllerSwipeListener implements View.OnTouchListener {

        private GestureDetectorCompat detector;

        BottomControllerSwipeListener() {
            detector = new GestureDetectorCompat(getApplicationContext(), new OnSwipeListener() {
                @Override
                public boolean onSwipe(Direction direction) {
                    // show song details screen on upswiping bottom controller
                    if (direction == Direction.up) {
                        Intent intent = new Intent(MainActivity.this, MainSongDetailActivity.class);
                        //put album id of current song as intent extra
                        //intent.putExtra("album_art", R.drawable.album_art_placeholder);
                        //put song_id
                        intent.putExtra("song_id", songId);
                        startActivity(intent);
                        return true;
                    }
                    return false;
                }
            });
        }

        //trigger the touch action
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            detector.onTouchEvent(motionEvent);
            return true;
        }
    }
}
