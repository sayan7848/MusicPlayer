package com.horntail.music.tmp3.activities;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;

import com.horntail.music.tmp3.R;
import com.horntail.music.tmp3.fragments.SongDetailFragment;

public class MainSongDetailActivity extends AppCompatActivity {

    public static final String TAG = "songDetailsActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_song_detail);
        //get song id from intent
        int id = getIntent().getIntExtra("song_id", R.drawable.album_art_placeholder);
        //pass the id to fragment
        Bundle bundle = new Bundle();
        bundle.putInt("song_id", id);
        Fragment fragment = new SongDetailFragment();
        fragment.setArguments(bundle);
        //bring in the fragment
        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction().
                    replace(R.id.activity_song_detail_container, fragment).commit();
        }
    }

}
