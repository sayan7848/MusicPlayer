package com.horntail.music.tmp3.fragments;


import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.view.GestureDetectorCompat;
import android.support.v7.graphics.Palette;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.horntail.music.tmp3.R;
import com.horntail.music.tmp3.constants.Statics;
import com.horntail.music.tmp3.helpers.OnSwipeListener;

import java.io.IOException;

import static com.horntail.music.tmp3.constants.Statics.getAlbumArtUri;
import static com.horntail.music.tmp3.constants.Statics.getComplimentColor;
import static com.horntail.music.tmp3.constants.Statics.getDominantColor;

/**
 * A simple {@link Fragment} subclass.
 */
public class SongDetailFragment extends Fragment {
    public static final String TAG = "songDetailFragment";
    private LinearLayout linearLayout;
    private ImageView album_art;
    private TextView songDetailsTotal, songDetailsElapsed;

    public SongDetailFragment() {
        // Required empty public constructor
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_song_detail, container, false);
    }

    @Override
    public void onViewCreated(final View view, Bundle savedInstanceState) {
        //initialize components
        linearLayout = view.findViewById(R.id.activity_song_detail);
        album_art = view.findViewById(R.id.album_art_song_detail);
        songDetailsTotal = view.findViewById(R.id.song_detail_total);
        songDetailsElapsed = view.findViewById(R.id.song_detail_elapsed);
        //get id of current song from intent
        int id = R.drawable.album_art_placeholder;
        if (getArguments() != null) {
            id = getArguments().getInt("song_id", R.drawable.album_art_placeholder);
        }
        //set background color
        setBackgroundColor(id);
        //set album_art resource
        setAlbumArt(id);
        //set gesture detector to linearLayout
        linearLayout.setOnTouchListener(new BottomControllerSwipeListener());
    }

    //set dominant and complimentary color from album art as background and text colors
    private void setBackgroundColor(int id) {
        Bitmap bitmap;
        try {
            bitmap = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), getAlbumArtUri(id));
        } catch (IOException ex) {
            bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.album_art_placeholder);
        }
        Palette colorPalette = Palette.from(bitmap).generate();
        /*linearLayout.setBackgroundColor(colorPalette.getDominantColor(Color.BLACK));
        songDetailsElapsed.setTextColor(colorPalette.getMutedColor(Color.WHITE));
        songDetailsTotal.setTextColor(colorPalette.getMutedColor(Color.WHITE));*/
        Palette.Swatch vibrantSwatch = colorPalette.getVibrantSwatch();
        if (vibrantSwatch != null) {
            linearLayout.setBackgroundColor(vibrantSwatch.getRgb());
            songDetailsElapsed.setTextColor(vibrantSwatch.getTitleTextColor());
            songDetailsTotal.setTextColor(vibrantSwatch.getTitleTextColor());
        } else {
            int dominant_color = getDominantColor(bitmap);
            int complimentary_color = getComplimentColor(dominant_color);
            linearLayout.setBackgroundColor(dominant_color);
            songDetailsElapsed.setTextColor(complimentary_color);
            songDetailsTotal.setTextColor(complimentary_color);
        }
    }

    //set album_art from id
    private void setAlbumArt(int id) {
        Bitmap bitmap;
        try {
            bitmap = MediaStore.Images.Media.getBitmap(getContext().getContentResolver(), getAlbumArtUri(id));
        } catch (IOException ex) {
            bitmap = BitmapFactory.decodeResource(getResources(), R.drawable.album_art_placeholder);
        }
        album_art.setImageBitmap(bitmap);
    }

    //custom upward swipe listener for bottom controller
    private class BottomControllerSwipeListener implements View.OnTouchListener {

        private GestureDetectorCompat detector;

        BottomControllerSwipeListener() {
            detector = new GestureDetectorCompat(getContext(), new OnSwipeListener() {
                @Override
                public boolean onSwipe(Direction direction) {
                    // show song queue screen on up swiping bottom controller
                    if (direction == Direction.up) {
                        Fragment fragment = new SongQueueFragment();
                        getActivity().getSupportFragmentManager().beginTransaction().addToBackStack(TAG).
                                replace(R.id.activity_song_detail_container, fragment).commit();
                        return true;
                    }
                    return false;
                }
            });
        }

        //trigger the touch action
        @Override
        public boolean onTouch(View view, MotionEvent motionEvent) {
            detector.onTouchEvent(motionEvent);
            return true;
        }
    }
}
