package com.horntail.music.tmp3.adapters;

import android.content.Context;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.horntail.music.tmp3.R;
import com.horntail.music.tmp3.constants.Statics;
import com.horntail.music.tmp3.data.Song;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

/**
 * Created by sayan on 9/8/17.
 */

public class SongAdapter extends RecyclerView.Adapter<SongAdapter.CustomViewHolder> {

    private Context context;
    private ArrayList<Song> songArrayList;

    //initialise the context and song arrayList
    public SongAdapter(Context context, ArrayList<Song> songArrayList) {
        this.context = context;
        this.songArrayList = songArrayList;
    }

    public SongAdapter(Context context) {
        this.context = context;
    }

    //create a new holder for songs
    @Override
    public CustomViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext()).inflate(R.layout.song, parent, false);
        return new CustomViewHolder(itemView);
    }

    //populate the holder with song data
    @Override
    public void onBindViewHolder(CustomViewHolder holder, int position) {
        if (songArrayList != null && songArrayList.size() > 0) {
            Song song = songArrayList.get(position);
            holder.title.setText(song.getName());
            holder.artist.setText(song.getArtist());
            holder.duration.setText(Statics.formatter(song.getDuration()));
            Uri path = Statics.getAlbumArtUri(song.getAlbumId());
            Picasso.with(context).load(path).placeholder(R.drawable.album_art_placeholder).error(R.drawable.album_art_placeholder).fit().centerCrop().into(holder.albumThumb);
        }
    }

    //return number of songs
    @Override
    public int getItemCount() {
        return songArrayList == null ? 0 : songArrayList.size();
    }

    //replace old songArrayList with new one
    public void changeData(ArrayList<Song> newSongArrayList) {
        if (newSongArrayList == songArrayList)
            return;
        if (newSongArrayList != null) {
            if (songArrayList != null) {
                songArrayList.clear();
            }
            songArrayList = newSongArrayList;
            notifyDataSetChanged();
        } else {
            notifyItemRangeRemoved(0, getItemCount());
            if (songArrayList != null) {
                songArrayList.clear();
            }
            songArrayList = null;
        }
    }

    //custom holder for song class
    static class CustomViewHolder extends RecyclerView.ViewHolder {
        TextView title, artist, duration;
        ImageView albumThumb;

        CustomViewHolder(View view) {
            super(view);
            title = view.findViewById(R.id.song_title);
            artist = view.findViewById(R.id.song_artist);
            duration = view.findViewById(R.id.song_duration);
            albumThumb = view.findViewById(R.id.song_thumbnail);
        }
    }
}
