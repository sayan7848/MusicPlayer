package com.horntail.music.tmp3.fragments;


import android.database.Cursor;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v4.app.Fragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.horntail.music.tmp3.R;
import com.horntail.music.tmp3.activities.MainActivity;
import com.horntail.music.tmp3.adapters.SongAdapter;
import com.horntail.music.tmp3.constants.Statics;
import com.horntail.music.tmp3.data.Song;
import com.horntail.music.tmp3.helpers.ItemClickSupport;
import com.horntail.music.tmp3.helpers.SongContainer;

import java.util.ArrayList;

/**
 * A simple {@link Fragment} subclass.
 */
public class MainSongFragment extends Fragment implements LoaderManager.LoaderCallbacks<Cursor> {

    private static final String TAG = "mainSongFragment";
    private SongAdapter songAdapter;
    private ArrayList<Song> songArrayList;
    private ArrayList<Integer> songIdArrayList;

    public MainSongFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        songAdapter = new SongAdapter(getContext());
        return inflater.inflate(R.layout.fragment_main_song, container, false);
    }

    @Override
    public void onViewCreated(final View view, Bundle savedInstanceState) {
        RecyclerView songList = view.findViewById(R.id.song_list_main);
        //create a layout manager for recycler view
        RecyclerView.LayoutManager layoutManager = new LinearLayoutManager(getContext());
        songList.setLayoutManager(layoutManager);
        songList.setItemAnimator(new DefaultItemAnimator());
        //set adapter to recycler view
        songList.setAdapter(songAdapter);
        //set item click listener to recycler view
        ItemClickSupport.addTo(songList).setOnItemClickListener(new ItemClickSupport.OnItemClickListener() {
            @Override
            public void onItemClicked(RecyclerView recyclerView, int position, View v) {
                Log.e(TAG, songArrayList.get(position).getData());
                //toggle visibility of bottom controller
                ((MainActivity) getActivity()).showBottomController(songArrayList.get(position).getName(),
                        songArrayList.get(position).getAlbumId());
            }
        });
        //load songs from media store
        getLoaderManager().initLoader(0, null, this);
    }

    @Override
    public Loader<Cursor> onCreateLoader(int id, Bundle args) {
        //return a cursor to all songs in device
        return new CursorLoader(getContext(), Statics.allSongsUri, Statics.songColumns,
                null, null, MediaStore.Audio.Media.TITLE);
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor data) {
        //get song arrayList and songPath arrayList from loader created in previous step
        SongContainer songContainer = Statics.songLoader(data);
        songArrayList = songContainer.songArrayList;
        //songIdArrayList = songContainer.songIdArrayList;
        TextView noSong = getView().findViewById(R.id.no_song_main);
        //show message if no song found on device
        if (songArrayList == null || songArrayList.size() == 0) {
            noSong.setVisibility(View.VISIBLE);
        } else {
            noSong.setVisibility(View.GONE);
        }
        //dismiss the progress bar
        getView().findViewById(R.id.song_loading_main).setVisibility(View.GONE);
        //populate the adapter
        if (songArrayList != null && songArrayList.size() > 0) {
            songAdapter.changeData(songArrayList);
            songAdapter.notifyDataSetChanged();
        }
    }

    //clear the present song list on loader reset
    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        songAdapter.changeData(null);
        if (songArrayList != null) {
            songArrayList.clear();
        }
        songArrayList = null;
        songAdapter.notifyDataSetChanged();
    }
}
