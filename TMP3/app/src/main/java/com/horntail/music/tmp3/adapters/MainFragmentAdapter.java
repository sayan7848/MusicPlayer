package com.horntail.music.tmp3.adapters;

import android.content.Context;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import com.horntail.music.tmp3.R;
import com.horntail.music.tmp3.fragments.MainAlbumFragment;
import com.horntail.music.tmp3.fragments.MainArtistFragment;
import com.horntail.music.tmp3.fragments.MainGenreFragment;
import com.horntail.music.tmp3.fragments.MainSongFragment;

/**
 * Created by sayan on 9/8/17.
 */

public class MainFragmentAdapter extends FragmentPagerAdapter {
    private static String[] tabTitles;

    //get value of tab titles in constructor
    public MainFragmentAdapter(Context context, FragmentManager fragmentManager) {
        super(fragmentManager);
        tabTitles = context.getResources().getStringArray(R.array.main_fragment_tabs);
    }

    //return tab corresponding to position
    @Override
    public Fragment getItem(int position) {
        Fragment fragment;
        switch (position) {
            case 0:
                fragment = new MainSongFragment();
                break;
            case 1:
                fragment = new MainAlbumFragment();
                break;
            case 2:
                fragment = new MainArtistFragment();
                break;
            case 3:
                fragment = new MainGenreFragment();
                break;
            default:
                fragment = null;
                break;
        }
        return fragment;
    }

    //return count of tabs
    @Override
    public int getCount() {
        return tabTitles.length;
    }

    //return tabTitles based on position
    @Override
    public CharSequence getPageTitle(int position) {
        // Generate title based on item position
        return tabTitles[position];
    }
}
