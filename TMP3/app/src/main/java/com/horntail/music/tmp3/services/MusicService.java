package com.horntail.music.tmp3.services;

import android.app.Service;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.Binder;
import android.os.Handler;
import android.os.IBinder;

import com.horntail.music.tmp3.constants.Statics;
import com.horntail.music.tmp3.data.Song;

import java.util.ArrayList;

public class MusicService extends Service implements MediaPlayer.OnCompletionListener, MediaPlayer.OnPreparedListener, MediaPlayer.OnErrorListener{

    //song data
    private ArrayList<Integer> songIdArrayList;
    private int currentSongIndex;
    private Song currentSong;

    //media player controls
    public static final String setSongList = "setSongList";
    public static final String setSongIndex = "setSongIndex";
    public static final String playSong = "playSong";
    public static final String killService = "killService";
    public static final String pauseSong = "pauseSong";
    public static final String resumeSong = "resumeSong";
    public static final String prevSong = "prevSong";
    public static final String nextSong = "nextSong";
    public static final String stopPlayer = "stopPlayer";
    public static final String togglePlayback = "toggle";

    //broadcast actions
    public static final String newSongStarted = "newSongStarted";

    //TAG for debugging
    private final static String TAG = "MusicService";

    //notification id
    private final static int NOT_ID = 7848;

    //binder to the present service
    private final IBinder musicBinder = new MusicBinder();

    //the actual mediaPlayer
    private MediaPlayer mediaPlayer = null;

    //handler for displaying on main thread
    private Handler handler = null;

    //custom binder for music service
    public class MusicBinder extends Binder {
        public MusicService getService() {
            return MusicService.this;
        }
    }

    public MusicService() {
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onCompletion(MediaPlayer mediaPlayer) {

    }

    @Override
    public boolean onError(MediaPlayer mediaPlayer, int i, int i1) {
        return false;
    }

    @Override
    public void onPrepared(MediaPlayer mediaPlayer) {

    }

    private void getCurrentSong() {
        currentSong = Statics.getSongFromId(getApplicationContext(), songIdArrayList.get(currentSongIndex));
    }
}
