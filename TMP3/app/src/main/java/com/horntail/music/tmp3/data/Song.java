package com.horntail.music.tmp3.data;

/**
 * Created by sayan on 9/8/17.
 */

public class Song {

    private long duration;
    private int albumId, id;
    private String name;
    private String artist;
    private String data;
    private String album;

    public Song(int id, long duration, int albumId, String name, String artist, String data, String album) {
        this.id = id;
        this.duration = duration;
        this.albumId = albumId;
        this.name = name;
        this.artist = artist;
        this.data = data;
        this.album = album;
    }

    public Song(Song song) {
        this.id = song.getId();
        this.duration = song.getDuration();
        this.albumId = song.getAlbumId();
        this.name = song.getName();
        this.artist = song.getArtist();
        this.data = song.getData();
        this.album = song.getAlbum();
    }


    public String getAlbum() {
        return album;
    }

    public int getId() {
        return id;
    }

    public long getDuration() {
        return duration;
    }

    public int getAlbumId() {
        return albumId;
    }

    public String getName() {
        return name;
    }

    public String getArtist() {
        return artist;
    }

    public String getData() {
        return data;
    }
}
