package com.horntail.music.tmp3.fragments;


import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.horntail.music.tmp3.R;
import com.horntail.music.tmp3.adapters.MainFragmentAdapter;
import com.ogaclejapan.smarttablayout.SmartTabLayout;

/**
 * A simple {@link Fragment} subclass.
 */
public class MainFragment extends Fragment {


    public MainFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_main, container, false);
    }

    @Override
    public void onViewCreated(View view, Bundle bundle) {
        MainFragmentAdapter adapter = new MainFragmentAdapter(getContext(), getChildFragmentManager());
        ViewPager viewPager = view.findViewById(R.id.view_pager_main);
        //reference to tabLayout
        SmartTabLayout tabLayout = view.findViewById(R.id.sliding_tabs_main);
        //install the adapter
        viewPager.setAdapter(adapter);
        //set tabLayout with viewpager
        tabLayout.setViewPager(viewPager);
    }

}
