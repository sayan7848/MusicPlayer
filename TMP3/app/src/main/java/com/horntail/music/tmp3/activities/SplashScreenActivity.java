package com.horntail.music.tmp3.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;

import com.horntail.music.tmp3.R;

public class SplashScreenActivity extends AppCompatActivity {

    private final short writePerm = 1;

    //execution starts here
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_splash_screen);
        //request for permission
        requestForPermission();
    }

    //requests for required permissions
    private void requestForPermission() {
        //check if app has required permissions granted
        if (ContextCompat.checkSelfPermission(getApplicationContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
            //should we show an explanation for associated permissions?
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.WRITE_EXTERNAL_STORAGE)) {
                Snackbar.make(findViewById(R.id.activity_splash_screen), getString(R.string.perm_explain_storage), Snackbar.LENGTH_SHORT).show();

            }
            //no explanation required, request straight away
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.WRITE_EXTERNAL_STORAGE}, writePerm);
        }
        //app already has permission
        else {
            resumeApp();
        }

    }

    //start main app
    private void resumeApp() {
        Runnable runnable = new Runnable() {
            @Override
            public void run() {
                //intent to mainActivity
                Intent intent = new Intent(getApplicationContext(), MainActivity.class);
                //clear activity stack
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                //send intent
                startActivity(intent);
                //finish current activity
                finish();
            }
        };
        new Handler().post(runnable);
    }

    //Callback for requestPermissions
    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case writePerm:
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //permission has been granted, continue with app
                    resumeApp();
                } else {
                    //no write permission, cannot continue :-(
                    Snackbar.make(findViewById(R.id.activity_splash_screen), getString(R.string.missing_perm), Snackbar.LENGTH_INDEFINITE).setAction("OK", new View.OnClickListener() {
                        @Override
                        public void onClick(View v) {
                            finish();
                        }
                    }).show();
                }
                break;
        }
    }
}
