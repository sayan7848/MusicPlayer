package com.horntail.music.tmp3.helpers;

import com.horntail.music.tmp3.data.Song;

import java.util.ArrayList;

/**
 * Created by sayan on 9/9/17.
 */

public class SongContainer {
    public ArrayList<Song> songArrayList;
    public ArrayList<Integer> songIdArrayList;

    public SongContainer(ArrayList<Song> songArrayList, ArrayList<Integer> songIdArrayList) {
        this.songArrayList = songArrayList;
        this.songIdArrayList = songIdArrayList;
    }
}
