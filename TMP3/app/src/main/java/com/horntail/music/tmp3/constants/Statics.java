package com.horntail.music.tmp3.constants;

import android.content.ContentUris;
import android.content.Context;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Color;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.provider.BaseColumns;
import android.provider.MediaStore;

import com.horntail.music.tmp3.data.Song;
import com.horntail.music.tmp3.helpers.SongContainer;

import java.util.ArrayList;
import java.util.Locale;
import java.util.concurrent.TimeUnit;

/**
 * Created by sayan on 9/8/17.
 */

public class Statics {

    public static final Uri allSongsUri = MediaStore.Audio.Media.EXTERNAL_CONTENT_URI;

    //various columns to be selected from mediaStore database for songs
    public static final String[] songColumns = new String[]{
            MediaStore.Audio.Media._ID,
            MediaStore.Audio.Media.ALBUM_ID,
            MediaStore.Audio.Media.TITLE,
            MediaStore.Audio.Media.DURATION,
            MediaStore.Audio.Media.ARTIST,
            MediaStore.Audio.Media.DATA,
            MediaStore.Audio.Media.ALBUM
    };

    //various columns to be selected from mediaStore database for albums
    public static final String[] albumColumns = new String[]{
            BaseColumns._ID,
            MediaStore.Audio.AlbumColumns.ALBUM,
            MediaStore.Audio.AlbumColumns.NUMBER_OF_SONGS,
            MediaStore.Audio.AlbumColumns.ARTIST,
    };

    //various columns to be selected from mediaStore database for genres
    public static final String[] genreColumns = new String[]{
            MediaStore.Audio.Genres._ID,
            MediaStore.Audio.Genres.NAME,
    };

    //various columns to be selected from mediaStore database for playLists
    public static final String[] playlistColumns = new String[]{
            MediaStore.Audio.Playlists._ID,
            MediaStore.Audio.Playlists.NAME,
    };

    //various columns to be selected from mediaStore database for artists
    public static final String[] artistColumns = new String[]{
            MediaStore.Audio.Artists._ID,
            MediaStore.Audio.Artists.ARTIST,
            MediaStore.Audio.Artists.NUMBER_OF_TRACKS,

    };

    //format long  value to hrs:min:sec
    public static String formatter(long value) {
        return String.format(Locale.getDefault(), "%02d:%02d:%02d",
                TimeUnit.MILLISECONDS.toHours(value),
                TimeUnit.MILLISECONDS.toMinutes(value) -
                        TimeUnit.HOURS.toMinutes(TimeUnit.MILLISECONDS.toHours(value)),
                TimeUnit.MILLISECONDS.toSeconds(value) -
                        TimeUnit.MINUTES.toSeconds(TimeUnit.MILLISECONDS.toMinutes(value)));
    }

    //returns an arrayList with songs retrieved from supplied cursors
    public static SongContainer songLoader(Cursor data) {
        ArrayList<Song> songArrayList = null;
        ArrayList<Integer> songIdArrayList = null;
        int id, albumId;
        long duration;
        String title, artist, path, album;
        if (data != null && data.moveToFirst()) {
            songArrayList = new ArrayList<>();
            songIdArrayList = new ArrayList<>();
            do {
                id = data.getInt(0);
                albumId = data.getInt(1);
                title = data.getString(2);
                duration = data.getLong(3);
                artist = data.getString(4);
                path = data.getString(5);
                album = data.getString(6);
                songArrayList.add(new Song(id, duration, albumId, title, artist, path, album));
                songIdArrayList.add(id);
            } while (data.moveToNext());
        }
        return new SongContainer(songArrayList, songIdArrayList);
    }

    //get uri to albumArt from albumId
    public static Uri getAlbumArtUri(long albumId) {
        Uri sArtworkUri = Uri
                .parse("content://media/external/audio/albumart");
        return ContentUris.withAppendedId(sArtworkUri, albumId);
    }

    //get dominant colour from bitmap
    public static int getDominantColor(Bitmap bitmap) {
        Bitmap newBitmap = Bitmap.createScaledBitmap(bitmap, 1, 1, true);
        final int color = newBitmap.getPixel(0, 0);
        newBitmap.recycle();
        return color;
    }

    //get dominant colour from drawable
    public static int getDominantColor(Drawable drawable) {
        Bitmap bitmap = ((BitmapDrawable) drawable).getBitmap();
        return getDominantColor(bitmap);
    }

    //get dominant colour from drawable resource id
    public static int getDominantColor(Context context, int id) {
        Bitmap bitmap = BitmapFactory.decodeResource(context.getResources(), id);
        return getDominantColor(bitmap);
    }

    //get dominant color from albumid
    public static int getDominantColor(Context context, int id, int defaultId) {
        int color;
        try {
            Bitmap bitmap = MediaStore.Images.Media.getBitmap(context.getContentResolver(), getAlbumArtUri(id));
            color = getDominantColor(bitmap);
        } catch (Exception ex) {
            color = getDominantColor(context, defaultId);
        }
        return color;
    }

    //get Song from id
    public static Song getSongFromId(Context context, int id) {
        String selection = MediaStore.Audio.Media._ID + "=?";
        String[] selectionArgs = new String[] {"" + id};
        Song song = null;
        Cursor mediaCursor = context.getContentResolver().query(allSongsUri, songColumns, selection, selectionArgs, null);
        int songId, albumId;
        long duration;
        String title, artist, path, album;
        if (mediaCursor != null && mediaCursor.getCount() > 0) {
            mediaCursor.moveToPosition(0);
            songId = mediaCursor.getInt(0);
            albumId = mediaCursor.getInt(1);
            title = mediaCursor.getString(2);
            duration = mediaCursor.getLong(3);
            artist = mediaCursor.getString(4);
            path = mediaCursor.getString(5);
            album = mediaCursor.getString(6);
            song = new Song(songId, duration, albumId, title, artist, path, album);
        }
        if (mediaCursor != null) {
            mediaCursor.close();
        }
        return song;
    }

    public static int getComplimentColor(int color) {
        // get existing colors
        int alpha = Color.alpha(color);
        int red = Color.red(color);
        int blue = Color.blue(color);
        int green = Color.green(color);

        // find compliments
        red = (~red) & 0xff;
        blue = (~blue) & 0xff;
        green = (~green) & 0xff;

        return Color.argb(alpha, red, green, blue);
    }
}
